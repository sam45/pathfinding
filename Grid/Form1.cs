﻿using Pathfinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Grid {
    public partial class Form1 : Form {
        private int numOfCells = 10; // default number of cells per row/column
        private Node[,] nodes; // array of all nodes
        private int cellSize; // size of a single cell
        private Graphics g; // graphics object
        private Pathfinder finder = new Pathfinder(); // instance of the logical class to determine the path

        // start with the start node
        private bool isSetStart = true;
        private bool isSetEnd = false;
        private bool isSetWall = false;
        private bool error = false;
        private bool showDebug = false;
        private bool isPathFound = false;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Form1() {
            InitializeComponent();
            this.nodes = new Node[numOfCells, numOfCells];
            this.cellSize = pictureBox1.Width / numOfCells;
            this.labelInfo.Text = "Set the start node first";
            this.buttonFindPath.Enabled = false;
        }

        /// <summary>
        /// Draw a grid with nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_Paint(object sender, PaintEventArgs e) {
            // init graphics object
            g = pictureBox1.CreateGraphics();

            // draw all (empty) squares
            for (int y = 0; y < numOfCells; ++y) {
                for (int x = 0; x < numOfCells; ++x) {
                    e.Graphics.DrawRectangle(new Pen(Color.Black), new Rectangle(x * cellSize, y * cellSize, cellSize, cellSize));

                    // add new node                
                    nodes[y, x] = new Node((x * cellSize + cellSize / 2), (y * cellSize + cellSize / 2), new NodeNumber(y, x));
                }
            }
        }

        /// <summary>
        /// When clicked on a square on the grid, set node mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e) {
            if (!isPathFound) {
                int x = e.X; // clicked x-coordinate
                int y = e.Y; // clicked y-coordinate
                Node ClickedNode = null; // clicked node

                // get the clicked node
                foreach (Node node in nodes) {
                    // add half the cellsize because the coordinates are from the middle instead of left under
                    if (x < node.X + cellSize / 2 && x > node.X - cellSize / 2 && y < node.Y + cellSize / 2 &&
                        y > node.Y - cellSize / 2) {
                        ClickedNode = node;
                    }
                }

                if (ClickedNode != null) {
                    // start node
                    if (isSetStart) {
                        // set new label and next setting
                        this.labelInfo.Text = "Set the end node";
                        isSetStart = false;
                        isSetEnd = true;
                        // delete previous start node and asign a new one
                        foreach (Node n in nodes) {
                            if (n.Mode == Modes.Start && ClickedNode.Mode == Modes.Free) {
                                ClearNode(n);
                            }
                        }
                        // asign a new start node if the clicked node is free
                        if (ClickedNode.Mode == Modes.Free) {
                            SetNode(ClickedNode, Color.Green);
                            ClickedNode.Mode = Modes.Start;
                            finder.StartNode = ClickedNode;
                        }

                        // end node
                    } else if (isSetEnd) {
                        this.labelInfo.Text = "Set the wall nodes";
                        isSetEnd = false;
                        isSetWall = true;
                        buttonFindPath.Enabled = true;

                        // delete previous end node 
                        foreach (Node n in nodes) {
                            if (n.Mode == Modes.End && ClickedNode.Mode == Modes.Free) {
                                ClearNode(n);
                            }
                        }
                        // asign a new one if the clicked node is free
                        if (ClickedNode.Mode == Modes.Free) {
                            SetNode(ClickedNode, Color.Red);
                            ClickedNode.Mode = Modes.End;
                            finder.EndNode = ClickedNode;
                        }

                        // wall node
                    } else if (isSetWall) {
                        // delete wall node if already set
                        if (ClickedNode.Mode == Modes.Wall) {
                            ClearNode(ClickedNode);
                        } else if (ClickedNode.Mode == Modes.Free) {
                            // set new wall node 
                            SetNode(ClickedNode, Color.Blue);
                            ClickedNode.Mode = Modes.Wall;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Call the path calculation method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFindPath_Click(object sender, EventArgs e) {
            // disable settings
            labelInfo.Text = "";
            checkBox1.Enabled = false;
            numericUpDown1.Enabled = false;
            buttonStartNode.Enabled = false;
            buttonEndNode.Enabled = false;
            buttonWall.Enabled = false;

            // initialize
            Stopwatch stopWatch;
            finder.Init(nodes);
            bool end = true; // check to see when the end node is found
            try {
                // loop through the path finding algoritm until the end node is found, abort if there isn't a path possible
                do {
                    stopWatch = new Stopwatch();
                    stopWatch.Start();

                    end = finder.Start();
                } while (end);
                // find the final list of nodes to use to get to the path
                finder.FindPath();

                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                Console.WriteLine("RunTime " + stopWatch.Elapsed);

            } catch (Exception) {
                error = true;
                labelInfo.Text = "No path could be found.";
                labelInfo.ForeColor = Color.Red;
            }

            if (!error) {
                if (showDebug) {
                    ShowDebugValues();
                }
                isPathFound = true;
                foreach (Node node in finder.Path) {
                    Pen p = new Pen(Color.SpringGreen, 3f);
                    g.FillEllipse(new SolidBrush(Color.DarkTurquoise), new Rectangle(node.X - cellSize / 6, node.Y - cellSize / 6, cellSize / 3, cellSize / 3));
                }
            }
        }

        /// <summary>
        /// Change the look and mode of a node to free
        /// </summary>
        /// <param name="n"></param>
        private void ClearNode(Node n) {
            Pen p = new Pen(Color.Black);
            n.Mode = Modes.Free;
            SolidBrush b = new SolidBrush(Color.WhiteSmoke);
            g.FillRectangle(b, new Rectangle((n.X - cellSize / 2), (n.Y - cellSize / 2), cellSize, cellSize));
            g.DrawRectangle(p, new Rectangle((n.X - cellSize / 2), (n.Y - cellSize / 2), cellSize, cellSize));
        }

        /// <summary>
        /// Change the look and mode of a node to free
        /// </summary>
        /// <param name="n"></param>
        private void SetNode(Node n, Color color) {
            Pen p = new Pen(Color.Black);
            SolidBrush b = new SolidBrush(color);
            g.FillRectangle(b, new Rectangle((n.X - cellSize / 2), (n.Y - cellSize / 2), cellSize, cellSize));
            g.DrawRectangle(p, new Rectangle((n.X - cellSize / 2), (n.Y - cellSize / 2), cellSize, cellSize));
        }

        /// <summary>
        /// Reset all objects and redraw picturebox
        /// </summary>
        private void Reset() {
            // start with the start node
            isSetStart = true;
            isSetEnd = false;
            isSetWall = false;
            error = false;
            checkBox1.Enabled = true;
            numericUpDown1.Enabled = true;
            buttonStartNode.Enabled = true;
            buttonEndNode.Enabled = true;
            buttonWall.Enabled = true;
            isPathFound = false;
            labelInfo.Text = "Set the start node first";
            buttonFindPath.Enabled = false;
            nodes = new Node[numOfCells, numOfCells];
            finder = new Pathfinder();
            pictureBox1.Invalidate();
        }

        /// <summary>
        /// Show calculated costs on the nodes
        /// </summary>
        private void ShowDebugValues() {
            // show F, G, H values on screen
            foreach (Node node in finder.OpenList) {
                using (Font font = new Font("Arial", 7)) {
                    g.DrawString(node.FValue.ToString(), font, Brushes.DarkBlue, new Point(node.X - cellSize / 2, node.Y - cellSize / 2));
                    g.DrawString(node.GValue.ToString(), font, Brushes.DarkBlue, new Point(node.X - cellSize / 2, node.Y + 9));
                    g.DrawString(node.HValue.ToString(), font, Brushes.DarkBlue, new Point(node.X, node.Y + 9));
                }
            }
        }

        #region UI elements

        private void buttonStartNode_Click(object sender, EventArgs e) {
            isSetStart = true;
            isSetEnd = false;
            isSetWall = false;
        }

        private void buttonEndNode_Click(object sender, EventArgs e) {
            isSetStart = false;
            isSetEnd = true;
            isSetWall = false;
        }

        private void buttonWall_Click(object sender, EventArgs e) {
            isSetStart = false;
            isSetEnd = false;
            isSetWall = true;
        }

        private void buttonReset_Click(object sender, EventArgs e) {
            Reset();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e) {
            numOfCells = (int)numericUpDown1.Value;
            cellSize = pictureBox1.Width / numOfCells;
            Reset();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Environment.Exit(0);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            AboutBox1 about = new AboutBox1();
            about.Show();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            if (checkBox1.Checked) {
                showDebug = true;
            } else {
                showDebug = false;
            }
        }

        #endregion
    }
}