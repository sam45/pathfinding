﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathfinding {
    /// <summary>
    /// A node is one part, one square of the grid
    /// </summary>
    public class Node {
        // fields
        private int x; // coordinates are the middle of the square
        private int y; // coordinates are the middle of the square
        private int hValue; // heuristic value
        private int gValue; // movement cost
        private int fValue; // total cost
        private Node parentNode; // parent node
        private Modes mode;
        private NodeNumber number;

        // properties
        #region properties

        public int HValue {
            get { return hValue; }
            set { hValue = value; }
        }

        public int GValue {
            get { return gValue; }
            set { gValue = value; }
        }

        public int FValue {
            get { return fValue; }
            set { fValue = value; }
        }

        public int Y {
            get { return y; }
        }

        public int X {
            get { return x; }
        }

        public Modes Mode {
            get { return mode; }
            set { mode = value; }
        }

        public NodeNumber Number {
            get { return number; }
            set { number = value; }
        }

        public Node ParentNode {
            get { return parentNode; }
            set { parentNode = value; }
        }

        #endregion

        // Default constructor
        public Node() {
            this.hValue = 0;
            this.gValue = 0;
            this.fValue = 0;
            this.ParentNode = null;
            this.Mode = Modes.Free;
        }

        // Non-default constructor
        public Node(int x, int y, NodeNumber number) {
            this.x = x;
            this.y = y;
            this.hValue = 0;
            this.gValue = 0;
            this.fValue = 0;
            this.ParentNode = null;
            this.Mode = Modes.Free;
            this.number = number;
        }

        /// <summary>
        /// Calculate the total cost(f) 
        /// by adding the heuristic value (h) and the movement cost (g)
        /// </summary>
        public void CaluclateFValue() {
            this.fValue = this.gValue + this.hValue;
        }

        /// <summary>
        /// H value: Calculate the distance to the end node
        /// Manhattan method: add the number of squares moved vertically and horizontally to reach the end node
        /// </summary>
        /// <param name="node"></param>
        public void CalculateHValue(Node endNode, int baseCost) {
            int steps = 0;
            NodeNumber position = new NodeNumber(number.Row, number.Column);
            // move rows/columns until the node is on the same location as the end node
            while (position.Row < endNode.number.Row) {
                position.Row++;
                steps++;
            }
            while (position.Row > endNode.number.Row) {
                position.Row--;
                steps++;
            }

            while (position.Column < endNode.number.Column) {
                position.Column++;
                steps++;
            }
            while (position.Column > endNode.number.Column) {
                position.Column--;
                steps++;
            }

            // multiply this value with the base cost
            this.hValue = steps * baseCost;
        }
    }
}