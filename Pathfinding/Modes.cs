﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathfinding {
    /// <summary>
    /// Enumeration with all possible states/modes of a node
    /// </summary>
    public enum Modes : sbyte { Start, End, Wall, Free };
}