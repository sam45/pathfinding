﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathfinding {
    public class Pathfinder {
        // the cost of a movement
        private const int BASE_MOV_COST = 10; // Base movement cost for horizontal & vertical movement.
        private const int DIAGONAL_MOV_COST = 15; // Base movement cost for diagonal movement.
        // lists of nodes
        private List<Node> openList = new List<Node>(); // Nodes that have not been checked.
        private List<Node> closedList = new List<Node>(); // Nodes that have been checked.
        private List<Node> path = new List<Node>(); // List of nodes that trace back from the end point to the start point
        // start and end node
        private Node startNode = null; // Position to trace a path from.
        private Node endNode = null; // Position that needs to be found.
        // all nodes in the grid
        private Node[,] nodes;

        #region properties

        public Node StartNode {
            get { return startNode; }
            set { startNode = value; }
        }

        public Node EndNode {
            get { return endNode; }
            set { endNode = value; }
        }

        public List<Node> OpenList {
            get { return openList; }
            set { openList = value; }
        }

        public List<Node> ClosedList {
            get { return closedList; }
            set { closedList = value; }
        }

        public List<Node> Path {
            get { return path; }
            set { path = value; }
        }

        #endregion

        /// <summary>
        /// Start the search with the start node and it's adjacent nodes
        /// </summary>
        /// <param name="nodes"></param>
        public void Init(Node[,] nodes) {
            this.nodes = nodes; // get the list of nodes from the GUI

            // find start node and end note
            Node startNode = null;
            Node endNode = null;
            foreach (Node n in nodes) {
                if (n.Mode == Modes.Start) {
                    startNode = n;
                }
                if (n.Mode == Modes.End) {
                    endNode = n;
                }
            }

            // check the adjancent nodes, add new ones to the open list, determine parent node and calculate G values
            CheckAdjacentNotes(startNode);

            // add startnode to closed list and remove it from the open list
            closedList.Add(startNode);
            openList.Remove(startNode);

            // calculate H cost of all nodes in the open list
            foreach (Node node in openList) {
                node.CalculateHValue(endNode, BASE_MOV_COST);
                node.CaluclateFValue();
            }
        }

        /// <summary>
        /// Continue the search with the open list of nodes and the node with the lowest F-value
        /// </summary>
        /// <returns></returns>
        public bool Start() {
            // use the node with the lowest F cost
            Node lowestFNode = null;
            // if there isn't a node in the open list, that means that there's wasn't a path found -> abort
            try {
                lowestFNode = openList[0];
            } catch (Exception) {
                return false;
                throw new Exception("No path found");
            }
            foreach (Node node in openList) {
                if (node.FValue < lowestFNode.FValue) {
                    lowestFNode = node;
                }
            }

            // drop the node with the lowest F cost from the open list and add it to the closed list
            closedList.Add(lowestFNode);
            openList.Remove(lowestFNode);

            // check the adjancent nodes, add new ones to the open list, determine parent node and calculate G values
            CheckAdjacentNotes(lowestFNode);

            // calculate H cost
            foreach (Node node in openList) {
                node.CalculateHValue(endNode, BASE_MOV_COST);
                node.CaluclateFValue();
            }

            // if the node with the lowest F value was the end node -> stop repeating this method
            if (lowestFNode.Mode == Modes.End) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check the adjacent notes and add the free ones to the open list
        /// </summary>
        /// <param name="currentNode"></param>
        public void CheckAdjacentNotes(Node currentNode) {
            foreach (Node n in nodes) {
                // if row number is equal - horizontal
                if (currentNode.Number.Row == n.Number.Row) {
                    // and the column number is equal or one more or one less
                    if (currentNode.Number.Column == n.Number.Column || currentNode.Number.Column - 1 == n.Number.Column ||
                        currentNode.Number.Column + 1 == n.Number.Column) {

                        // horizontal and vertical use the default base cost 
                        LogicAdjacentNotes(currentNode, n, true);
                    }

                    // if row column is equal - vertical
                } else if (currentNode.Number.Column == n.Number.Column) {
                    // and the row number is equal or one more or one less
                    if (currentNode.Number.Row == n.Number.Row || currentNode.Number.Row - 1 == n.Number.Row ||
                        currentNode.Number.Row + 1 == n.Number.Row) {

                        // horizontal and vertical use the default base cost 
                        LogicAdjacentNotes(currentNode, n, true);
                    }

                    // row and column one less or one more - diagonal LD and RU
                } else if (currentNode.Number.Row - 1 == n.Number.Row && currentNode.Number.Column - 1 == n.Number.Column ||
                           currentNode.Number.Row + 1 == n.Number.Row && currentNode.Number.Column + 1 == n.Number.Column) {

                    // diagonal uses the more expensive movement cost
                    LogicAdjacentNotes(currentNode, n, false);

                    // row one more, column one les and the other way around - diagonal LU and RD
                } else if (currentNode.Number.Row + 1 == n.Number.Row && currentNode.Number.Column - 1 == n.Number.Column ||
                           currentNode.Number.Row - 1 == n.Number.Row && currentNode.Number.Column + 1 == n.Number.Column) {

                    // diagonal uses the more expensive movement cost
                    LogicAdjacentNotes(currentNode, n, false);
                }
            }
        }

        /// <summary>
        /// Find the final path with only the nodes that lead directly to the end node
        /// </summary>
        public void FindPath() {
            path = new List<Node>();

            // start from the end node and move backwards to the start node
            Node previous = endNode.ParentNode;
            path.Add(previous);
            while (previous != startNode) {
                // don't add the start node to the list
                if (previous.ParentNode != startNode) {
                    path.Add(previous.ParentNode);
                }
                previous = previous.ParentNode;
            }
        }

        /// <summary>
        /// Calculate the G values and set the parent nodes
        /// </summary>
        /// <param name="currentNode"></param>
        /// <param name="n"></param>
        /// <param name="base_cost"></param>
        public void LogicAdjacentNotes(Node currentNode, Node n, bool base_cost) {
            // add the node if it's free and not in the closed list
            if (n.Mode != Modes.Wall && !closedList.Contains(n)) {
                // add to open list and add it's parent node, if not already on the open list
                if (openList.Contains(n)) {
                    // check if the G score for that square is lower than the if we use the current square.
                    // if lower, change parent to selected square if not, do nothing
                    int gvalTest = base_cost ? n.ParentNode.GValue + BASE_MOV_COST : n.ParentNode.GValue + DIAGONAL_MOV_COST;
                    if (gvalTest < n.GValue) {
                        n.ParentNode = currentNode;
                        n.CalculateHValue(endNode, BASE_MOV_COST);
                        n.CaluclateFValue();
                    }
                    else {
                        //
                    }
                } else {
                    openList.Add(n);
                    n.ParentNode = currentNode;
                    n.GValue = base_cost ? n.ParentNode.GValue + BASE_MOV_COST : n.ParentNode.GValue + DIAGONAL_MOV_COST;
                    n.CaluclateFValue();
                }
            }
        }
    }
}