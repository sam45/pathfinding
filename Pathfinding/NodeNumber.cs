﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathfinding {
    /// <summary>
    /// Class which contains the logical numeration of nodes in a grid
    /// </summary>
    public class NodeNumber {
        private int row;
        private int column;

        /// <summary>
        /// Non-default constructor
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        public NodeNumber(int row, int column) {
            this.row = row;
            this.column = column;
        }

        # region properties

        public int Row {
            get { return row; }
            set { row = value; }
        }

        public int Column {
            get { return column; }
            set { column = value; }
        }

        #endregion
    }
}
